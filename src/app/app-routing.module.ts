import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecondGraphComponent } from './second-graph/second-graph.component';
import { FirstGraphComponent } from './first-graph/first-graph.component';
import { ThirdGraphComponent } from './third-graph/third-graph.component';
import { ForthGraphComponent } from './forth-graph/forth-graph.component';
import { FifthGraphComponent } from './fifth-graph/fifth-graph.component';
import { SixthGraphComponent } from './sixth-graph/sixth-graph.component';

const routes: Routes = [
  { path: '', redirectTo: '/fourth', pathMatch: 'full' },
  { path: 'first', component: FirstGraphComponent },
  { path: 'second', component: SecondGraphComponent },
  { path: 'third', component: ThirdGraphComponent },
  { path: 'fourth', component: ForthGraphComponent },
  { path: 'fifth', component: FifthGraphComponent },
  { path: 'sixth', component: SixthGraphComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
