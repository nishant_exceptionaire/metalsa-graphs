import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SecondGraphComponent } from './second-graph/second-graph.component';
import { FirstGraphComponent } from './first-graph/first-graph.component';
import { ThirdGraphComponent } from './third-graph/third-graph.component';
import { ForthGraphComponent } from './forth-graph/forth-graph.component';
import { FifthGraphComponent } from './fifth-graph/fifth-graph.component';
import { FusionChartsModule } from 'angular-fusioncharts';
import { HighchartsChartModule } from 'highcharts-angular';

// Load FusionCharts
import * as FusionCharts from 'fusioncharts';
// Load Charts module
import * as Charts from 'fusioncharts/fusioncharts.charts';
// Load fusion theme
import * as PowerCharts from 'fusioncharts/fusioncharts.powercharts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as Highcharts from 'highcharts';
import { SixthGraphComponent } from './sixth-graph/sixth-graph.component';

// Add dependencies to FusionChartsModule
FusionChartsModule.fcRoot(FusionCharts, Charts, PowerCharts,FusionTheme)

@NgModule({
  declarations: [
    AppComponent,
    SecondGraphComponent,
    FirstGraphComponent,
    ThirdGraphComponent,
    ForthGraphComponent,
    FifthGraphComponent,
    SixthGraphComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FusionChartsModule,
    HighchartsChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
