import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fifth-graph',
  templateUrl: './fifth-graph.component.html',
  styleUrls: ['./fifth-graph.component.css']
})
export class FifthGraphComponent implements OnInit {
 width = 800;
  height = 600;
  type = "scatter";
  dataFormat = "json";
  dataSource = {
      "chart": {
        "theme": "ocean",
        "drawQuadrant": "1",
        "quadrantLabelTL": "Strechability",
        "quadrantLabelTR": "Strechability + Deep Draw",
        "quadrantLabelBR": "Deep Draw",
        "caption": "Formability Graph",
        "xAxisName": "Lankford Coefficient",
        "yAxisName": "Strain Hardnening Exponent (n)",
        "xAxisMinValue": "0.70",
        "xAxisMaxValue": "1.30"
      },
      "categories": [{
        "category": [{
          "x": "0.70",
          "label": "0.70",
          "showverticalline": "1"
        }, {
          "x": "0.80",
          "label": "0.80",
          "showverticalline": "1"
        }, {
          "x": "0.90",
          "label": "0.90",
          "showverticalline": "1"
        }, {
          "x": "1.00",
          "label": "1.00",
          "showverticalline": "1"
        }, {
          "x": "1.10",
          "label": "1.10",
          "showverticalline": "1"
        }, {
          "x": "1.20",
          "label": "1.20",
          "showverticalline": "1"
        }, {
          "x": "1.30",
          "label": "1.30",
          "showverticalline": "1"
        }]
      }],
      "dataset": [{
        "seriesname": "METIN001",
        "anchorBgColor": "#addef7",
        "anchorBordercolor": "#17a1e8",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.81",
          "y": "0.180"
        }]
      }, {
        "seriesname": "METIN002",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.88",
          "y": "0.210"
        }]
      },{
        "seriesname": "METIN003",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.89",
          "y": "0.175"
        }]
      },{
        "seriesname": "METIN004",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.87",
          "y": "0.145"
        }]
      },
      {
        "seriesname": "METIN005",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.92",
          "y": "0.115"
        }]
      },
      {
        "seriesname": "METIN006",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "1.04",
          "y": "0.009"
        }]
      },
      {
        "seriesname": "METIN007",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "1.12",
          "y": "0.100"
        }]
      },
      {
        "seriesname": "METIN008",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.98",
          "y": "0.180"
        }]
      },
      {
        "seriesname": "METIN009",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.91",
          "y": "0.180"
        }]
      },
      {
        "seriesname": "METIN010",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "0.94",
          "y": "0.110"
        }]
      }]
    }

  constructor() { }

  ngOnInit() {
  }

}
