import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-forth-graph',
  templateUrl: './forth-graph.component.html',
  styleUrls: ['./forth-graph.component.css']
})
export class ForthGraphComponent implements OnInit {

  Highcharts = Highcharts;
  chartOptions = {
    chart: {
        type: 'spline',
       	inverted: true
    },
    title: {
        text: 'Stress-Strain Curve'
    },
    xAxis: {
        reversed: false,
        title: {
            enabled: true,
            text: 'Stress[Mpa]'
        },
        labels: {
            format: '{value}'
        },
        maxPadding: 0.05,
        showLastLabel: true
    },
    yAxis: {
        title: {
            text: 'Strain'
        },
        labels: {
            format: '{value}'
        },
        lineWidth: 2
    },
    plotOptions: {
      spline: {
          marker: {
              enabled: false
          }
      }
    },
    series: [{
        name: 'METIN001',
        data: [[0, 0], [128, 0.00061], [256, 0.001219], [384, 0.001829], [512, 0.002491],[544, 0.002733], [576, 0.003104], [608, 0.003764], [640, 0.005048],[660, 0.00644], [680, 0.008594], [700, 0.011912], [720, 0.016988], [740, 0.024688],[760, 0.036263], [780, 0.078935], [800, 0.116119], [820, 0.17]],

    },{
        name: 'METIN002',
        data: [[0, 0], [86, 0.0004095], [172, 0.0008191], [258,	0.0012329], [344	,0.001775],
        [365.5,	0.0020241], [387	,0.0024067], [408.5, 0.003025], [430, 0.0040476], [450, 0.0055967777],
        [470, 0.0080628],[490, 0.0119445], [510, 0.0179731],[530, 0.0272038], [550, 0.0411381],
        [570,	0.061884],[590,	0.0923652759],[610,	0.136590997],[630,	0.2]
    ]

    }]

};

  constructor() { }

  ngOnInit() {
  }

}
