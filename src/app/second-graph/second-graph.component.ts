import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-graph',
  templateUrl: './second-graph.component.html',
  styleUrls: ['./second-graph.component.css']
})
export class SecondGraphComponent implements OnInit {
 
  width = 800;
  height = 600;
  type = "scatter";
  dataFormat = "json";
  dataSource = {
      "chart": {
        "theme": "ocean",
        "caption": "Formability Graph",
        "xAxisName": "Lankford Coefficient",
        "yAxisName": "Strain Hardnening Exponent (n)",
        "yAxisMax":"1.0000"
      },
      "categories": [{
        "category": [{
          "x": "1",
          "label": "1.E+00"
        }, {
          "x": "10",
          "label": "1.E+01"
        }, {
          "x": "100",
          "label": "1.E+02"
        }, {
          "x": "1000",
          "label": "1.E+03"
        }, {
          "x": "10000",
          "label": "1.E+04"
        }, {
          "x": "100000",
          "label": "1.E+05"
        }, {
          "x": "1000000",
          "label": "1.E+06"
        }, {
          "x": "10000000",
          "label": "1.E+07"
        }]
      }],
      "dataset": [{
        "seriesname": "METIN001",
        "anchorBgColor": "#addef7",
        "anchorBordercolor": "#17a1e8",
        "anchorBorderThickness": "6",
        "data": [{
          "x": "1",
          "y": "0.1990"
        }]
      }]
    }
  constructor() { }

  ngOnInit() {
  }

}
