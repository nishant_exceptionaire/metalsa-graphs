import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sixth-graph',
  templateUrl: './sixth-graph.component.html',
  styleUrls: ['./sixth-graph.component.css']
})
export class SixthGraphComponent implements OnInit {
  width = 650;
  height = 350;
  type = "scatter";
  dataFormat = "json";
  dataSource = {
    "chart": {
      // "caption": "Sales of Cold drinks & Ice-cream vs Temperature",
      // "subcaption": "Los Angeles Topanga",
      "xaxisname": "Life Reversals (2Nf)",
      "yaxisname": "Strain",
      "xaxisminvalue": "1",
      "xaxismaxvalue": "1000000",
      "ynumberprefix": "",
      "xnumbersuffix": "",
      "theme": "fusion",
      "showyonx": "1"
    },
    "categories": [{
      "category": [{
        "x": "1",
        "label": "1",
        "showverticalline": "0"
      }, {
        "x": "10",
        "label": "10",
        "showverticalline": "1"
      }, {
        "x": "100",
        "label": "100",
        "showverticalline": "1"
      }, {
        "x": "1000",
        "label": "1000",
        "showverticalline": "1"
      }, {
        "x": "10000",
        "label": "10000",
        "showverticalline": "1"
      }, {
        "x": "100000",
        "label": "100000",
        "showverticalline": "1"
      }
      , {
        "x": "1000000",
        "label": "1000000",
        "showverticalline": "1"
      }]
    }],
    "dataset": [{
      "seriesname": "Total Strain Amplitude",
      "showregressionline": "1",
      "regressionLineAlpha": "70",
      "data": [{
          "x": "1",
          "y": "0.0060"
        },
        {
          "x": "3932",
          "y": "0.0060"
        },
        {
          "x": "5580",
          "y": "0.0060"
        },
        {
          "x": "6740",
          "y": "0.0040"
        },
        {
          "x": "15378",
          "y": "0.0040"
        },
        {
          "x": "17350",
          "y": "0.0040"
        },
        {
          "x": "20608",
          "y": "0.0020"
        },
        {
          "x": "114000",
          "y": "0.0020"
        },
        {
          "x": "117650",
          "y": "0.0020"
        },
        {
          "x": "130228",
          "y": "0.0020"
        },
        {
          "x": "394904",
          "y": "0.0015"
        },
        {
          "x": "400020",
          "y": "0.0015"
        },
        {
          "x": "2000000",
          "y": "0.0012"
        }
      ]
    }, {
      "seriesname": "Elastic Strain Amplitude",
      "showregressionline": "1",
      "regressionlinecolor": "#808080",
      "anchorbgcolor": "#34C3BD",
      "anchorbordercolor": "#808080",
      "anchorsides": "1",
      "anchorradius": "1",
      "data": [{
          "x": "1",
          "y": "0.0043"
        },
        {
          "x": "3932",
          "y": "0.0018"
        },
        {
          "x": "5580",
          "y": "0.0018"
        },
        {
          "x": "6740",
          "y": "0.0018"
        },
        {
          "x": "15378",
          "y": "0.0016"
        },
        {
          "x": "17350",
          "y": "0.0016"
        },
        {
          "x": "20608",
          "y": "0.0016"
        },
        {
          "x": "114000",
          "y": "0.0013"
        },
        {
          "x": "117650",
          "y": "0.0013"
        },
        {
          "x": "130228",
          "y": "0.0013"
        },
        {
          "x": "394904",
          "y": "0.0012"
        },
        {
          "x": "400020",
          "y": "0.0012"
        },
        {
          "x": "2000000",
          "y": "0.0010"
        }
      ]
    }]
  }
  constructor() { }

  ngOnInit() {  
  }

}
